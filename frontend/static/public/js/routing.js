



var app = angular.module("radio", ["ngRoute"]);



app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "home.html",
        title:"Internet Public Radio"
    })

    .when("/shows", {
        templateUrl : "shows.html",
        title:"Internet Public Radio - Shows"
    })
    .when("/shows/:show_name", {
        templateUrl : "individual-show.html",
        title:"Internet Public Radio - Shows"
    })

    .when("/residents", {
        templateUrl : "residents.html",
        title:"Internet Public Radio - Residents"
    })
    .when("/residents/:resident_name", {
        templateUrl : "individual-resident.html",
        title:"Internet Public Radio - Residents"
    })

    .when("/guests", {
        templateUrl : "guests.html",
        title:"Internet Public Radio - Guests"
    })

    .when("/events", {
        templateUrl : "events.html",
        title:"Internet Public Radio - Events"
    })

    .when('/tags/:tag', {
        templateUrl: "shows.html",
        title:"Internet Public Radio - Shows"
    })


    .when('/about', {
        templateUrl: "about.html",
        title:"Internet Public Radio - About"
    })

    .when('/schedule', {
        templateUrl: "schedule.html",
        title:"Internet Public Radio - Schedule"
    })
    ;


    $locationProvider.html5Mode({
        enabled:true
    });
    
});


app.run(['$rootScope', '$route', function($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function() {
        document.title = $route.current.title;
    });
}]);